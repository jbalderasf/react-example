import { configureStore } from '@reduxjs/toolkit'
import tvShowReducer from '../features/tvShows/tvShowsSlice'

export default configureStore({
  reducer: {
    tvShows: tvShowReducer
  }
})