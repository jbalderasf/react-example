export const fetchTvShows = async () => {
  const response = await fetch(
    'http://api.tvmaze.com/shows'
  );
  return await response.json(); 
}