import './App.scss';
import { TvShowApp } from './components/tvShowApp/tvShowApp'

function App() {
  return (
    <div className="App">
      <TvShowApp></TvShowApp>
    </div>
  );
}

export default App;
