import React, { useState } from 'react'

export function TvShowFilter(props) {
  const [filter, setFilter] = useState('');

  const handleChange = (ev) => {
    props.change(ev.target.value)
    setFilter(ev.target.value)
  } 

  return (
    <div>
      <input type="text" placeholder="Filter" value={filter} onChange={handleChange} />
    </div>
  )
}