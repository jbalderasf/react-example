import React, { useState, useEffect } from 'react'
import { setFavorite, removeFavorite } from '../../features/tvShows/tvShowsSlice'
import { useDispatch, useSelector } from 'react-redux'

export function Checkbox(props) {
  const { tvShow } = props
  const dispatch = useDispatch()
  const favorites = useSelector(state => state.tvShows.favorites);
  const [value, setValue] = useState(false);
  const onChange = () => {
    if(value) {
      const ok = window.confirm('Are you sure you want to delete this item?');
      if(ok) {
        setValue(!value)
      }
    } else {
      setValue(!value)
    }
  }

  useEffect(() => {
    if(value) {
      dispatch(setFavorite(tvShow.id))
    } else {
      dispatch(removeFavorite(tvShow.id))
    }
  }, [value]);

  useEffect(() => {
    setValue(favorites.includes(tvShow.id))
  }, [favorites]);

  return <input type="checkbox" checked={value} onChange={onChange} />
}