import React, { useEffect, useState } from 'react'
import './tvShowApp.scss';
import { TvShowFilter } from '../tvShowFilter/tvShowFilter';
import { TvShowList } from '../tvShowsList/tvShowList'
import { useTvShows, setFilter, loadTvShows, toggleViewFavorites, setFavorite, removeFavorite, loadFavorites } from '../../features/tvShows/tvShowsSlice'
import { useDispatch, useSelector } from 'react-redux'
import { fetchTvShows } from '../../app/api'
import ReactHtmlParser from 'react-html-parser';
import Modal from 'react-modal';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

export function TvShowApp() {
  const dispatch = useDispatch()
  const tvShowsList = useTvShows()
  const [modalIsOpen, setIsOpen] = useState(false);
  const [showDetails, setShowDetails] = useState({});
  const favorites = useSelector(state => state.tvShows.favorites);

  useEffect(() => {
    const calltvShows = async () => {
      const result = await fetchTvShows();
      dispatch(loadTvShows(result));
    };
    calltvShows();
  }, [dispatch]);

  useEffect(() => {
    if(showDetails.id) {
      if(showDetails.isFavorite) {
        dispatch(setFavorite(showDetails.id))
      } else {
        dispatch(removeFavorite(showDetails.id))
      }
    }
  }, [showDetails]);

  const filterChange = filter => {
    dispatch(setFilter(filter))
  }

  const onViewFavorites = () => {
    dispatch(toggleViewFavorites())
  }

  function openModal(tvShow) {
    setShowDetails({...tvShow, isFavorite: favorites.includes(tvShow.id)})
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  function addFavorite(id) {
    setShowDetails({
      ...showDetails,
      isFavorite: true
    })
  }

  function deleteFavorite(id) {
    const ok = window.confirm('Are you sure you want to delete this item?');
      if(ok) {
        setShowDetails({
          ...showDetails,
          isFavorite: false
        })
      }
    
  }

  function saveFavorites() {
    if (typeof(Storage) !== "undefined") {
      localStorage.setItem("myFavorites", JSON.stringify({
        favorites: favorites
      }));
    }
  }

  function loadLocalFavorites() {
    if (typeof(Storage) !== "undefined") {
      const store = localStorage.getItem("myFavorites")
      if(store){
        console.log(store)
        const o = JSON.parse(store)
        dispatch(loadFavorites(o.favorites))
      }
    
    }
  }

  return (
    <div className="tv-shows-app">
      <div className="tv-shows-app__head">
        My Tv Shows
      </div>
      <div className="tv-shows-app__filter">
        <TvShowFilter change={filterChange}></TvShowFilter>
      </div>
      <div className="tv-shows-app__filter">
        <button onClick={onViewFavorites}>View favorites</button>
      </div>
      <div className="tv-shows-app__list">
        <TvShowList list={tvShowsList} showDetail={openModal}></TvShowList>
      </div>
      <div className="tv-shows-app__footer">
      <button onClick={() => {saveFavorites()}}>Save favorites</button>
      <button onClick={() => {loadLocalFavorites()}}>Load favorites</button>
        <Modal
          isOpen={modalIsOpen}
          style={customStyles}
          contentLabel="Tv Show Detail"
        >
          <div className="modal-details">
            <div className="modal-details__close" onClick={closeModal}>X</div>
            <div className="modal-details__title">
              <h2>{showDetails.name}</h2>
            </div>
            <div className="modal-details__favorite">
              {showDetails.isFavorite ? 
                <div onClick={() => deleteFavorite(showDetails.id)}>Remove from favorites</div> : 
                <div onClick={() => addFavorite(showDetails.id)}>Add to favorites</div>}
            </div>
            <div className="modal-details__image">
              <img src={showDetails.image?.medium} width="160" height="160"></img>
            </div>
            <div className="modal-details__text">
              {ReactHtmlParser(showDetails.summary)} 
            </div>
            <div className="modal-details__link">
              <a href={showDetails.officialSite} target="_blank">site</a>
            </div>
          </div>
        </Modal>
      </div>
    </div>
  )
}