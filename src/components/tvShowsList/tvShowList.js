import React from 'react'
import './tvShowList.scss';
import { Checkbox } from '../tvShowItem/tvShowItem';

export function TvShowList(props) {
  const tvList = props.list.map((tvShow) => {
    return <div key={tvShow.id} className="tv-show-list__item">
      <div className="tv-show-list__item--left" onClick={() => { props.showDetail(tvShow)}}>
        <img src={tvShow.image.medium} width="60" height="60"></img>
        <span>{tvShow.name}</span>
         
      </div>
      <div className="tv-show-list__item--right">
        <Checkbox tvShow={tvShow}></Checkbox>
      </div>
    </div>
  })
  return (
    <div className="tv-show-list">
      {tvList}
    </div>
  )
}