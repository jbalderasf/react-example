import { createSlice } from '@reduxjs/toolkit'
import { useSelector } from 'react-redux'

export const tvShowsSlice = createSlice({
  name: 'tvShows',
  initialState: {
    filter: {
      value: ''
    },
    favorites: [],
    viewFavorites: {
      value: false
    },
    tvShows: {}
  },
  reducers: {
    loadTvShows: (state, action) => {
      state.tvShows = {}
      action.payload.map((item) => {
        state.tvShows[item.id] = item
      })
    },
    loadFavorites: (state, action) => {
      state.favorites = action.payload
    },
    setFilter: (state, action) => {
      state.filter = {
        value: action.payload
      }
    },
    setFavorite: (state, action) => {
      state.tvShows[action.payload].isFavorite = true;
      state.tvShows = {
        ...state.tvShows,
      }
      state.favorites = [
        ...state.favorites,
        action.payload
      ]
    },
    removeFavorite: (state, action) => {
      state.tvShows[action.payload].isFavorite = false;
      state.tvShows = {
        ...state.tvShows,
      }
      state.favorites = state.favorites.filter((item) => item !== action.payload)
    },
    toggleViewFavorites: state => {
      state.viewFavorites = {
        value: !state.viewFavorites.value
      }
    }
  }
})

export const { loadTvShows, setFilter, setFavorite, removeFavorite, toggleViewFavorites, loadFavorites } = tvShowsSlice.actions

export default tvShowsSlice.reducer

export function useTvShows() {
  return useSelector(state => {
    let result = [];
    Object.keys(state.tvShows.tvShows).map(item => {
      if(state.tvShows.filter.value) {
        if (state.tvShows.tvShows[item].name.startsWith(state.tvShows.filter.value)) {
          result = [
            ...result,
            state.tvShows.tvShows[item]
          ]
        }
      } else {
        result = [
          ...result,
          state.tvShows.tvShows[item]
        ]
      }
    });
    if(state.tvShows.viewFavorites.value) {
      result = result.filter(item => {
        return state.tvShows.favorites.includes(item.id)
      })
    }
    return result;
  })
}